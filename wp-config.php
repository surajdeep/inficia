<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'inficia');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'asdf1234%');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '{s<{6[e)~QY$N#%Qv$z8i~~21zamZ60s&HAS4+zB7::#<?Bgf?+` -C 1RY-YZ_O');
define('SECURE_AUTH_KEY',  'h318C{rEB`q;sG5D5r39f#F2041D~Y-LdPyA&51ng,H_X&)gZW<Wut#}NK61e5Em');
define('LOGGED_IN_KEY',    'GW>coKJ2hoC$w]2e>cnIR-!KZ5ijZKaFJ=i*qA3*Y }&IR,0!iN*ogDx!?`b~E@l');
define('NONCE_KEY',        ';9kdS4GX]w.?StAG$UIurkf*t}.=,w#A?+dB[UF=:ByHu_bKe=sq22v=HPC<PH@#');
define('AUTH_SALT',        'dh_txT97Pa,|XH^q@%[(pP4E;:xOcpTa,?|?uHB.9/lk[]Q}%`3 ~|HRCzdR OS9');
define('SECURE_AUTH_SALT', 'Avph%J8`.T>gZplK?>jiDfUz,W|y`hlR{K<>3K%LBg2z2FvRk?Ca%<*[s%Tdr2)}');
define('LOGGED_IN_SALT',   'NibXenJ?jb*; p(2%dt%hYW5}ZL_7nSsFA/*1G}t1)14Ps63|^N:7u)mLCs5ng=|');
define('NONCE_SALT',       '.?G}cZq`aBW%9%*tV+O0lI$b6<4s>w8Hn6]spm/5:z&20.XHe6:$Q+uAmW) 0]?x');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'infi_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);


define('FS_METHOD', 'direct');


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

